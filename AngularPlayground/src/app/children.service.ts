import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChildrenService {
  constructor() {
    
  }

  getChildren() : Observable<ChildGiftRequest[]>{
    return Observable.create([new ChildGiftRequest("Ignace", "Teddy bear"), new ChildGiftRequest("Glenn", "Flamingo")]);
  }

  getChildrenSecure() : Observable<ChildGiftRequestSecure[]>{
    return Observable.create([new ChildGiftRequestSecure("Ignace", "Teddy bear", false), new ChildGiftRequestSecure("Glenn", "Flamingo", true)]);
  }
}

export class ChildGiftRequest {
  name: string;
  toy: string;

  constructor(name: string, toy: string){
    this.name = name;
    this.toy = toy;
  }
}

export class ChildGiftRequestSecure extends ChildGiftRequest {
  isGood: boolean;

  constructor(name: string, toy: string, isGood: boolean){
    super(name, toy);
    this.isGood = isGood;
  }
}