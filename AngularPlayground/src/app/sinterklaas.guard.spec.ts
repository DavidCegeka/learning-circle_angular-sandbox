import { TestBed, async, inject } from '@angular/core/testing';

import { SinterklaasGuard } from './sinterklaas.guard';

describe('SinterklaasGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SinterklaasGuard]
    });
  });

  it('should ...', inject([SinterklaasGuard], (guard: SinterklaasGuard) => {
    expect(guard).toBeTruthy();
  }));
});
