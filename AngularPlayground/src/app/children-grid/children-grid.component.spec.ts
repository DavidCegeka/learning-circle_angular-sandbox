import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildrenGridComponent } from './children-grid.component';

describe('ChildrenGridComponent', () => {
  let component: ChildrenGridComponent;
  let fixture: ComponentFixture<ChildrenGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildrenGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildrenGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
