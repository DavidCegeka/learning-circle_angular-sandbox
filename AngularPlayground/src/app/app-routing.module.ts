import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChildrenGridComponent } from 'src/app/children-grid/children-grid.component';
import { SinterklaasGridComponent } from 'src/app/sinterklaas-grid/sinterklaas-grid.component';
import { SinterklaasGuard } from 'src/app/sinterklaas.guard';

const routes: Routes = [{ path: '', component: ChildrenGridComponent },
                        { path: 'Sinterklaas', component: SinterklaasGridComponent, canActivate: [SinterklaasGuard] }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
