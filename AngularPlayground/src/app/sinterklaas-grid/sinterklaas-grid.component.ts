import { Component, OnInit } from '@angular/core';
import { ChildrenService } from 'src/app/children.service';

@Component({
  selector: 'app-sinterklaas-grid',
  templateUrl: './sinterklaas-grid.component.html',
  styleUrls: ['./sinterklaas-grid.component.css']
})
// Protected by Sinterklaas guard --> can see children, gifts & good/bad
export class SinterklaasGridComponent implements OnInit {
  childrenRequests : ChildRequest[];

  constructor(private childrenService: ChildrenService) { }

  ngOnInit() {
  }

}
