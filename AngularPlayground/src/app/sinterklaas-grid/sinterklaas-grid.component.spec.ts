import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinterklaasGridComponent } from './sinterklaas-grid.component';

describe('SinterklaasGridComponent', () => {
  let component: SinterklaasGridComponent;
  let fixture: ComponentFixture<SinterklaasGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinterklaasGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinterklaasGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
